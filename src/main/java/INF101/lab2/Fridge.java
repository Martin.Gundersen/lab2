package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    int maxItems = 20;
    ArrayList<FridgeItem> items = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        
        return items.size();
    }

    @Override
    public int totalSize() {
        return maxItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(items.size()<maxItems){
            items.add(item);
            return true;
        } else return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(!items.contains(item)){
            throw new NoSuchElementException();
        }  
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> removed = new ArrayList<>();
        for(FridgeItem item : items){
            if(item.hasExpired()) removed.add(item);
        }
        items.removeAll(removed);
        return removed;
    }
    
}
